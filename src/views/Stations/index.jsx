import React, { useState, useLayoutEffect } from 'react';
import { IoLogoWhatsapp, IoMdMail } from 'react-icons/io';
import {
  CarrouselSection,
  InvoicesSection,
  InvoicesText,
  SectionTitle,
  StationTextContainer,
  StrongText,
  StationText,
  StationItem,
  StationLink,
  StationImg,
  StationLinkContainer,
} from './styles'; // Import Swiper styles
import { Footer } from '../../components/Footer';
import { yellowSecondary } from '../../assets/styles/colors';
import SwiperCore, { Autoplay, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import StationImage from '../../assets/images/estacion.jpg';
import Popup from 'reactjs-popup';
import { InvoiceMenu } from '../../components/InvoiceMenu';

import '../../assets/styles/swiper.css';

// install Swiper modules
SwiperCore.use([Autoplay, A11y]);

const contentStyle = {
  background: 'rgba(232,191,19,0)',
  width: '100vw',
  height: '100vh',
  border: 'none',
};

export const Stations = () => {
  const [items, setItems] = useState(5);

  useLayoutEffect(() => {
    function updateSize() {
      if (window.innerWidth > 1440) {
        setItems(5);
      }
      if (window.innerWidth <= 1440) {
        setItems(4);
      }
      if (window.innerWidth <= 1080) {
        setItems(3);
      }
      if (window.innerWidth <= 768) {
        setItems(2);
      }
      if (window.innerWidth <= 580) {
        setItems(2);
      }
      if (window.innerWidth <= 375) {
        setItems(1);
      }
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return (
    <>
      <CarrouselSection>
        <SectionTitle>
          Estaciones de <StrongText>Petro Darka</StrongText>
        </SectionTitle>
        <Swiper
          autoHeight
          spaceBetween={30}
          loop
          slidesPerView={items}
          freeMode={true}
          autoplay={{
            delay: 1200,
            disableOnInteraction: false,
          }}
          className="mySwiper"
          centeredSlides={true}
          speed={6500}
          freeModeMomentum={false}
        >
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Av. Base aérea militar # 330</StationText>
                <StationText>Zapopan, Jalisco</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href="https://w.app/Z73mzR">33 1888 0589</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:base.aerea@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink href="https://goo.gl/maps/MQL2sych3FyJK6qc9" rel="noopener noreferrer" target="_blank">
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Av. Alcalde # 2157</StationText>
                <StationText>Guadalajara, Jalisco</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href="https://w.app/NFAqvy">33 1941 1400</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:servicioextrarapidotransito@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink href="https://goo.gl/maps/CGBq2qobCcGxmDDe9" rel="noopener noreferrer" target="_blank">
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Carretera Guadalajara - Jocotepec Km. 60.5</StationText>
                <StationText>Ajijic Chapala, Jalisco</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href="https://w.app/raYv9D">33 2162 3361</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:estacionlamojonera@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink href="https://www.google.com/maps/dir//45822+Jocotepec,+Jal./@20.2922459,-103.3895277,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x842f4380d6778791:0xd0ed65d2b0af7bc0!2m2!1d-103.307126!2d20.2922652?entry=ttu">
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Carretera Encarnación - San Juan Km. 15</StationText>
                <StationText>San Sebastián Del Álamo, Jalisco</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href="tel:4757429559">475 7429 559</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:gasolineriasansebas@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink href="https://goo.gl/maps/3rYsDBwLLKrHw43W9" rel="noopener noreferrer" target="_blank">
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Blv Francisco J Mujica S/N</StationText>
                <StationText>Tingüindín, Michoacán</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href=" https://w.app/BAizr1">354 118 2323</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:exprestinguindin@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink href="https://goo.gl/maps/mjWXxBZUNCtgy272A" rel="noopener noreferrer" target="_blank">
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
          <SwiperSlide>
            <StationItem>
              <StationImg src={StationImage} alt="" />
              <StationTextContainer>
                <StationText>Calle Chicharo 5454</StationText>
                <StationText>Zapopan, Jalisco</StationText>
                <StationLinkContainer>
                  <IoLogoWhatsapp />
                  <StationLink href="tel:3336251021">33 3625 1021</StationLink>
                </StationLinkContainer>
                <StationLinkContainer>
                  <IoMdMail></IoMdMail>
                  <StationLink href="mailto:servicioextrarapido@hotmail.com">Enviar e-mail</StationLink>
                </StationLinkContainer>
                <StationLink
                  href="https://www.bing.com/maps?osid=150d12af-288c-46c2-86cf-9a50fd97d254&cp=20.763022~-103.340128&lvl=20&imgid=66b2bbea-df7f-4be3-941a-95a5c0b08cec&v=2&sV=2&form=S00027"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  Ver estación
                </StationLink>
              </StationTextContainer>
            </StationItem>
          </SwiperSlide>
        </Swiper>
      </CarrouselSection>

      <InvoicesSection>
        <Popup
          modal
          overlayStyle={{ background: 'rgba(232,191,19,1)' }}
          contentStyle={contentStyle}
          closeOnDocumentClick={false}
          trigger={(open) => <InvoicesText open={open}>FACTURA TU SERVICIO </InvoicesText>}
        >
          {(close) => <InvoiceMenu close={close} />}
        </Popup>
      </InvoicesSection>
      <Footer color={yellowSecondary} />
    </>
  );
};
