import React from "react";
import './index.css'
import whatsapp from './img/whatsapp.svg'

export default function WhatsAppButton() {
    return <a href="https://wa.link/edwbzo" className="whatsAppButton">
            <img src={whatsapp} alt="" />
    </a>
}