import React from 'react';
import CancelIcon from '../../assets/images/icons/cancel.png';
import { MenuContainer, MenuTitle, CloseContainer, StationLink, MenuRow, Icon } from './styles';

export const InvoiceMenu = ({ close }) => {
  return (
    <>
      <CloseContainer>
        <Icon onClick={close} src={CancelIcon} />
      </CloseContainer>
      <MenuContainer>
        <MenuTitle>SELECCIONA TU ESTACIÓN</MenuTitle>
        <div>
          <MenuRow>
            <StationLink rel="noopener noreferrer" target="_blank" href="http://sba.noip.us:83/controlgasfe/">
              AVIACIÓN
            </StationLink>
            <StationLink rel="noopener noreferrer" target="_blank" href="http://p02244extrarapido.ddns.net:83/ControlGASFE/FEInicio.aspx">
              ALCALDE
            </StationLink>
          </MenuRow>
          <MenuRow>
            <StationLink rel="noopener noreferrer" target="_blank" href="https://www.gasolineros.mx">
              JOCOTEPEC
            </StationLink>
            <StationLink rel="noopener noreferrer" target="_blank" href="http://gasstation.ddns.net:85/controlgasfe">
              SAN SEBASTÍAN
            </StationLink>
          </MenuRow>
          <MenuRow>
            <StationLink rel="noopener noreferrer" target="_blank" href="http://gl-operacion.com.mx/">
              TINGUINDIN
            </StationLink>
            <StationLink rel="noopener noreferrer" target="_blank" href="http://extrarapidomesa.ddns.net:83/controlgasfe/">
              MESA COLORADA
            </StationLink>
          </MenuRow>
        </div>
      </MenuContainer>
    </>
  );
};
